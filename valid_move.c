/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid_move.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 13:30:24 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:29:01 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		valid_move(t_all all, int row, int col)
{
	char	**small;

	if (ft_invalid_range(all, row, col))
		return (0);
	small = cut_map(all, row, col);
	if (place_piece(all, small))
	{
		free(small);
		return (1);
	}
	else
		free(small);
	return (0);
}
