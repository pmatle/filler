/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_least_distance.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/09 15:26:47 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:03:42 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

t_point		get_least_distance(t_moves *moves)
{
	t_point		point;
	t_moves		*tmp;
	int			dist;

	dist = moves->dist;
	point.row = moves->row;
	point.col = moves->col;
	tmp = moves;
	while (tmp != NULL)
	{
		if (tmp->dist < dist)
		{
			dist = tmp->dist;
			point.row = tmp->row;
			point.col = tmp->col;
		}
		tmp = tmp->next;
	}
	return (point);
}
