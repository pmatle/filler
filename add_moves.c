/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_moves.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 13:28:43 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 07:49:48 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int			add_moves(t_moves **head, int row, int col)
{
	t_moves		*new;

	if (!(new = (t_moves*)ft_memalloc(sizeof(*new))))
		return (0);
	new->row = row;
	new->col = col;
	new->next = (*head);
	(*head) = (new);
	return (1);
}
