/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_data.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/09 13:02:58 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:01:37 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		free_data(t_all all)
{
	t_data		*map;
	t_data		*piece;

	map = all.map;
	piece = all.piece;
	free_2d(map->data);
	free_2d(piece->data);
	free(map);
	free(piece);
	return (1);
}
