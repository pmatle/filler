/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start_algorithm.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 11:21:03 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:30:20 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int        start_algorithm(t_all all)
{
    t_moves     *moves;
    t_point     best;

    all.enemy = (all.me == 'X') ? 'O' : 'X';
    if (!(moves = get_moves(all)))
        ft_putendl("0 0");
    else
    {
        if (all.map->row == 15)
            best = method1(all, moves);
        else if (all.map->row == 24)
            best = method2(all, moves);
        else
            best = method3(all, moves);
        put_piece(best.row, best.col);
    }
    return (1);
}
