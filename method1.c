/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   method1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/10 17:41:52 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 09:09:45 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

t_moves		*method1_o(t_all all, t_moves *moves)
{
	t_moves		*tmp;

	tmp = moves;
	while (tmp != NULL)
	{
		if (all.map->data[6][12] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 6, 12);
		else if (all.map->data[12][0] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 12, 0);
		else
			tmp->dist = calculate_distance(tmp->row, tmp->col, 0, 16);
		tmp = tmp->next;
	}
	return (moves);
}

t_moves		*method1_x(t_all all, t_moves *moves)
{
	t_moves		*tmp;

	tmp = moves;
	while (tmp != NULL)
	{
		if (all.map->data[0][3] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 0, 3);
		else if (all.map->data[8][0] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 8, 0);
		else
			tmp->dist = calculate_distance(tmp->row, tmp->col, 0, 16);
		tmp = tmp->next;
	}
	return (moves);
}

t_point		method1(t_all all, t_moves *moves)
{
	if (all.me == 'O')
		moves = method1_o(all, moves);
	else if (all.me == 'X')
		moves = method1_x(all, moves);
	return (get_least_distance(moves));
}
