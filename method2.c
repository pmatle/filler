/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   method2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/12 09:54:02 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:05:47 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

t_moves		*method2_o(t_all all, t_moves *moves)
{
	t_moves		*tmp;

	tmp = moves;
	while (tmp != NULL)
	{
		if (all.map->data[15][18] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 15, 18);
		else if (all.map->data[0][19] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 0, 19);
		else if (all.map->data[0][0] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 0, 0);
		else
			tmp->dist = calculate_distance(tmp->row, tmp->col, 23, 28);
		tmp = tmp->next;
	}
	return (moves);
}

t_moves		*method2_x(t_all all, t_moves *moves)
{
	t_moves		*tmp;

	tmp = moves;
	while (tmp != NULL)
	{
		if (all.map->data[7][15] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 7, 15);
		else if (all.map->data[10][0] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 10, 0);
		else if (all.map->data[10][39] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 10, 39);
		else
			tmp->dist = calculate_distance(tmp->row, tmp->col, 21, 39);
		tmp = tmp->next;
	}
	return (moves);
}

t_point		method2(t_all all, t_moves *moves)
{
	if (all.me == 'O')
		moves = method2_o(all, moves);
	else
		moves = method2_x(all, moves);
	return (get_least_distance(moves));
}
