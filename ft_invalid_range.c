/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_invalid_range.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 18:55:13 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:02:35 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		ft_invalid_range(t_all all, int row, int col)
{
	t_data	*piece;
	t_data	*map;

	piece = all.piece;
	map = all.map;
	if ((row + piece->row) >= map->row)
		return (1);
	if ((col + piece->col) >= map->col)
		return (1);
	return (0);
}
