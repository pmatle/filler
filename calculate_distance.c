/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate_distance.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/09 15:45:09 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 07:51:40 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		calculate_distance(int y1, int x1, int y2, int x2)
{
	int		dist;

	dist = ABS((y1 - y2)) + abs((x1 - x2));
	return (dist);
}
