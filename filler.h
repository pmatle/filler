/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 18:21:01 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 07:58:06 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include <fcntl.h>
# include <sys/stat.h>
# include <sys/types.h>
# include "libft/includes/libft.h"

# define ABS(N) ((N < 0) ? -N : N)

typedef struct	s_data
{
	char	**data;
	int		row;
	int		col;
}				t_data;

typedef struct	s_point
{
	int		row;
	int		col;
}				t_point;

typedef struct	s_all
{
	t_data	*piece;
	t_data	*map;
	char	me;
	char	enemy;
	t_point	e_dir;
}				t_all;

typedef struct	s_moves
{
	int				row;
	int				col;
	int				dist;
	struct s_moves	*next;
}				t_moves;

t_data			*get_data(char *str, int fd, int flag);
t_data			*make_array(char *str);
int				free_2d(char **list);
int				start_algorithm(t_all all);
int				ft_count_chars(t_all all, char **small, char c);
int				ft_invalid_range(t_all all, int row, int col);
int				add_moves(t_moves **head, int row, int col);
int				valid_move(t_all all, int row, int col);
int				place_piece(t_all all, char **small);
char			**array_malloc(int row, int col);
char			**cut_map(t_all all, int row, int col);
t_moves			*get_moves(t_all all);
int				free_moves(t_moves *moves);
int				free_data(t_all all);
int				put_piece(int row, int col);
int				calculate_distance(int y1, int x1, int y2, int x2);
t_point			get_least_distance(t_moves *moves);
t_point			method1(t_all all, t_moves *moves);
t_point			method2(t_all all, t_moves *moves);
t_point			method3(t_all all, t_moves *moves);

#endif
