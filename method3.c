/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   method3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/12 13:10:22 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 10:08:34 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

t_moves		*method3_o(t_all all, t_moves *moves)
{
	t_moves		*tmp;

	tmp = moves;
	while (tmp != NULL)
	{
		if (all.map->data[6][12] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 6, 12);
		else if (all.map->data[0][67] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 0, 67);
		else if (all.map->data[28][76] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 28, 76);
		else
			tmp->dist = calculate_distance(tmp->row, tmp->col, 99, 0);
		tmp = tmp->next;
	}
	return (moves);
}

t_moves		*method3_x(t_all all, t_moves *moves)
{
	t_moves		*tmp;

	tmp = moves;
	while (tmp != NULL)
	{
		if (all.map->data[55][98] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 55, 98);
		else if (all.map->data[40][0] == '.')
			tmp->dist = calculate_distance(tmp->row, tmp->col, 40, 0);
		else
			tmp->dist = calculate_distance(tmp->row, tmp->col, 0, 0);
		tmp = tmp->next;
	}
	return (moves);
}

t_point		method3(t_all all, t_moves *moves)
{
	if (all.me == 'O')
		moves = method3_o(all, moves);
	else
		moves = method3_x(all, moves);
	return (get_least_distance(moves));
}
