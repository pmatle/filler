/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   place_piece.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 14:21:32 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:33:45 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		check_board(t_all all, char **small, int mine, int enemy)
{
	char	e;

	e = (all.me == 'X') ? 'O' : 'X';
	if (ft_count_chars(all, small, all.me) == (mine - 1))
	{
		if (enemy == ft_count_chars(all, small, e))
			return (1);
	}
	return (0);
}

int		place_piece(t_all all, char **small)
{
	int		x;
	int		y;
	int		mine;
	int		enemy;

	y = 0;
	mine = ft_count_chars(all, small, all.me);
	enemy = (all.me == 'X') ? ft_count_chars(all, small, 'O') :
		ft_count_chars(all, small, 'X');
	while (y < all.piece->row)
	{
		x = 0;
		while (x < all.piece->col)
		{
			if (all.piece->data[y][x] != '.')
				small[y][x] = all.piece->data[y][x];
			x++;
		}
		y++;
	}
	return (check_board(all, small, mine, enemy));
}
