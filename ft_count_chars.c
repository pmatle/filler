/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_count_chars.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/18 18:47:21 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:02:15 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		ft_count_chars(t_all all, char **map, char c)
{
	int		x;
	int		y;
	int		count;

	y = 0;
	count = 0;
	while (y < all.piece->row)
	{
		x = 0;
		while (x < all.piece->col)
		{
			if (map[y][x] == c || map[y][x] == (c + 32))
				count++;
			x++;
		}
		y++;
	}
	return (count);
}
