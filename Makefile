# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/10 12:43:33 by pmatle            #+#    #+#              #
#    Updated: 2017/12/13 07:56:12 by pmatle           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = filler

SRC = read.c get_data.c start_algorithm.c add_moves.c calculate_distance.c\
	  get_least_distance.c cut.c free_2d.c free_data.c ft_count_chars.c\
	  ft_invalid_range.c get_moves.c place_piece.c put_piece.c valid_move.c\
	  method1.c method2.c method3.c\

OBJ = read.o get_data.o start_algorithm.o add_moves.o calculate_distance.o\
	  get_least_distance.o cut.o free_2d.o free_data.o ft_count_chars.o\
	  ft_invalid_range.o get_moves.o place_piece.o put_piece.o valid_move.o\
	  method1.o method2.o method3.o\

CFLAGS = -Wall -Wextra -Werror

LIB = -L libft/ -lft

all: $(NAME)

$(NAME):
	make all -C libft/
	gcc -o $(NAME) $(LIB) $(CFLAGS) $(SRC)

clean:
	make clean -C libft/
	/bin/rm -f $(OBJ)

fclean: clean
	/bin/rm $(NAME)
	make fclean -C libft

re: fclean all
