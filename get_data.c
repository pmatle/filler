/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_data.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/08 11:07:15 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:03:00 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

char		**array_malloc(int row, int col)
{
	char	**array;
	int		x;

	x = 0;
	array = (char**)ft_memalloc(sizeof(char*) * row + 1);
	while (x < row)
	{
		array[x] = (char*)ft_memalloc(sizeof(char) * col + 1);
		x++;
	}
	array[x] = NULL;
	return (array);
}

t_data		*make_array(char *str)
{
	char	**list;
	t_data	*piece;
	int		x;

	x = 0;
	piece = NULL;
	list = ft_strsplit(str, ' ');
	if (!(piece = (t_data*)ft_memalloc(sizeof(*piece))))
		return (NULL);
	piece->col = ft_atoi(list[2]);
	piece->row = ft_atoi(list[1]);
	piece->data = array_malloc(piece->row, piece->col);
	return (piece);
}

t_data		*get_data(char *str, int fd, int flag)
{
	t_data		*piece;
	char		*line;
	int			count;
	int			y;
	int			x;

	piece = make_array(str);
	x = piece->row;
	count = 0;
	y = 0;
	if (flag == 1)
		get_next_line(fd, &line);
	while (x > 0)
	{
		get_next_line(fd, &line);
		count = 0;
		while (count < piece->col)
		{
			piece->data[y][count] = (flag == 1) ? line[count + 4] : line[count];
			count++;
		}
		x--;
		y++;
	}
	return (piece);
}
