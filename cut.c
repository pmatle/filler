/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cut.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/09 10:48:26 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 07:52:54 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

char	**cut_map(t_all all, int row, int col)
{
	int		x;
	int		y;
	char	**small;

	x = 0;
	small = array_malloc(all.piece->row, all.piece->col);
	while (x < all.piece->row)
	{
		y = 0;
		while (y < all.piece->col)
		{
			small[x][y] = all.map->data[row + x][col + y];
			y++;
		}
		x++;
	}
	return (small);
}
