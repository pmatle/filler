/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/05 14:52:39 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:32:03 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		get_player_infor(char *line)
{
	char	**list;
	int		x;

	list = ft_strsplit(line, ' ');
	if (ft_strequ(list[2], "p1"))
		x = 79;
	else
		x = 88;
	free_2d(list);
	return (x);
}

int		main(void)
{
	char			*line;
	static t_all	all;
	int				flag;

	while (get_next_line(0, &line))
	{
		flag = 0;
		if (ft_strstr(line, "[./filler]"))
			all.me = get_player_infor(line);
		if (ft_strstr(line, "Piece"))
		{
			if (!(all.piece = get_data(line, 0, 0)))
				return (0);
			flag = 1;
		}
		if (ft_strstr(line, "Plateau"))
		{
			if (!(all.map = get_data(line, 0, 1)))
				return (0);
		}
		if (flag == 1)
			start_algorithm(all);
		free(line);
	}
	return (0);
}
