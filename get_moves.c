/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_moves.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmatle <pmatle@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/09 12:07:55 by pmatle            #+#    #+#             */
/*   Updated: 2017/12/13 08:05:13 by pmatle           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

t_moves		*get_moves(t_all all)
{
	t_moves		*moves;
	int			x;
	int			y;

	y = 0;
	moves = NULL;
	while (y < all.map->row)
	{
		x = 0;
		while (x < all.map->col)
		{
			if (valid_move(all, y, x))
			{
				add_moves(&moves, y, x);
			}
			x++;
		}
		y++;
	}
	return (moves);
}

int			free_moves(t_moves *moves)
{
	t_moves		*temp;

	while (moves != NULL)
	{
		temp = moves;
		moves = moves->next;
		free(temp);
	}
	return (1);
}
